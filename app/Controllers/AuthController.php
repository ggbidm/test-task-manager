<?php

namespace App\Controllers;


use App\App;
use App\Controller;

class AuthController extends Controller
{
    public function login() {
        if (isset($_POST['login'])) {
            if (!check_csrf()) redirect('/');

            App::user()->load($_POST)->validate();

            if (!App::user()->isError()) {
                redirect('/');
            }
        }

        view('auth/login', [
            'user' => App::user(),
        ]);
    }

    public function logout() {
        App::user()->logout();
        redirect('/');
    }
}