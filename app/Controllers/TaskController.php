<?php

namespace App\Controllers;

use App\App;
use App\Controller;
use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $task = new Task();

        if (isset($_POST['add_task'])) {
            if (!check_csrf()) redirect('/');

            $task->load($_POST)->validate();

            if (!$task->isError()) {
                $task->save();
                App::setMessage('Задача успешно добавлена');
                $task = new Task();
            }
        }

        $orderBy = 'id';
        $order = 'ASC';

        if (isset($_GET['order_by'])) $orderBy = $_GET['order_by'];
        if (isset($_GET['order'])) $order = $_GET['order'];

        $pagination = Task::getPagination($orderBy, $order);

        view('tasks/list', [
            'pagination' => $pagination,
            'task' => $task
        ]);
    }

    public function edit($id) {
        if (!App::user()->isAuth) redirect('/login');
        $task = Task::getOne($id);
        if (!$task) redirect('/');

        if (isset($_POST['update_task'])) {
            if (!check_csrf()) redirect('/');

            $oldDescription = $task->description;
            $task->load($_POST)->validate();

            if (!$task->isError()) {
                if ($oldDescription != $task->description) {
                    $task->is_admin_updated = true;
                }

                $task->save();
                App::setMessage('Задача успешно сохранена');

                if ($_POST['update_task'] == 'Сохранить') {
                    redirect('/task/'.$task->id);
                } else {
                    redirect('/');
                }

            }
        }

        view('tasks/edit', [
            'task' => $task
        ]);
    }
}