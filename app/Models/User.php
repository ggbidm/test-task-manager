<?php

namespace App\Models;

use App\Model;

class User extends Model
{
    public $login = '';
    public $password = '';

    public $isAuth = false;

    // Данные администратора для входа
    protected $_login = 'admin';
    protected $_password = '123';

    protected $saveFields = ['login', 'password'];
    protected $fieldNames = [
        'login' => 'Логин',
        'password' => 'Пароль',
    ];

    protected $requiredFields = ['login', 'password'];
    protected $requiredErrTpl = 'Поле %s обязательно';

    public function validate()
    {
        parent::validate();

        // проверка
        if (!$this->login()) {
            $this->addError('error', 'Введены не верные данные');
        }

        return $this;
    }

    // попытка войти логина и пароля
    public function login() {
        if (($this->login == $this->_login) && ($this->password == $this->_password)) {
            $this->isAuth = true;
            $_SESSION['auth'] = true;
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        $_SESSION['auth'] = false;
        $this->isAuth = false;
    }
}