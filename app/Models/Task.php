<?php

namespace App\Models;

use App\Model;
use DB;

class Task extends Model
{
    const TABLE = 'tasks';

    public $user_name = '';
    public $email = '';
    public $description = '';
    public $status = 0;
    public $is_admin_updated = false;

    protected $saveFields = ['user_name', 'email', 'description', 'status'];
    protected $fields = ['user_name', 'email', 'description', 'status', 'is_admin_updated'];
    protected $fieldNames = [
        'user_name' => 'Имя пользователя',
        'email' => 'EMail',
        'description' => 'Текст задачи',
        'status' => 'Статус'
    ];
    protected $requiredFields = ['user_name', 'email', 'description'];
    protected $emailFields = ['email'];
    protected $requiredErrTpl = 'Поле %s обязательно.';
    protected $emailErrTpl = 'Поле %s должно быть email-адресом.';

    public static function getOne($id) {
        $model = new self();

        $row = DB::getOne(self::TABLE, $id);

        foreach ($row as $fieldName=>$fieldValue) {
            if (property_exists($model, $fieldName)) $model->{$fieldName} = $fieldValue;
        }

        return $model;
    }
}