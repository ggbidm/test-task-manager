<?php

namespace App;

class Controller
{

    public function __call($strMethod, $mParams)
    {
        if (!method_exists($this, $strMethod)) {
            view('404');
        } else {
            call_user_func_array(array(&$this, $strMethod));
        }
    }
}