<?php

namespace App;

use App\Models\User;

class App
{
    protected static $app;

    private function __construct() {}

    public static function user() {
        if (!isset(self::$app['user'])) {
            self::$app['user'] = new User();

            if (isset($_SESSION['auth']) && $_SESSION['auth'] == true) {
                self::$app['user']->isAuth = true;
            }
        }

        return self::$app['user'];
    }

    public static function getMessage() {
        if (isset($_SESSION['message']) && trim($_SESSION['message']) != '') {
            $message = $_SESSION['message'];
            $_SESSION['message'] = '';
            return $message;
        }
    }

    public static function setMessage($message) {
        $_SESSION['message'] = $message;
    }
}