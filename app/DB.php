<?php

class DB
{
    public static $db;

    private function __construct() {}

    public static function connect() {
        self::$db = new SafeMySQL([
            'host'      => 'localhost',
            'user'      => 'root',
            'pass'      => '',
            'db'        => 'test',
        ]);
    }

    public static function getCount($tableName, $where=1) {
        return DB::$db->getOne("SELECT COUNT(*) FROM ?n WHERE ?s", $tableName, $where);
    }

    public static function getAll($tableName, $orderBy, $order, $offset, $limit) {
        $sql = "SELECT * FROM ?n ORDER BY ?n ?p LIMIT ?i, ?i ;";

        return DB::$db->getAll($sql,
            $tableName,
            $orderBy,
            $order,
            $offset,
            $limit
        );
    }

    public static function getOne($tableName, $id) {
        return DB::$db->getRow('SELECT * FROM ?n WHERE id = ?i',
            $tableName,
            $id
        );
    }

    public static function update($tableName, $params, $where) {
        $sql = "UPDATE ?n SET ?u WHERE ?p;";
        DB::$db->query($sql, $tableName, $params, $where);
    }

    public static function insert($tableName, $params) {
        $sql = "INSERT INTO ?n SET ?u;";
        DB::$db->query($sql, $tableName, $params);
        return DB::$db->insertId();
    }
}