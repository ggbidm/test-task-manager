<?php

namespace App;

class Pagination
{
    private $pageNumGetName = 'page';
    private $view = 'pagination/default';

    public $perPage;
    public $page;
    public $countPages;
    public $count;
    public $list;
    public $offset;

    public $prevUrl;
    public $nextUrl;


    public function __construct($perPage, $count)
    {
        $this->perPage = $perPage;
        $this->count = $count;
        $this->offset = 0;

        if ($count > 0) {
            if (isset($_GET[$this->pageNumGetName]) && intval($_GET[$this->pageNumGetName]) > 0) {
                $this->page = intval($_GET[$this->pageNumGetName]);
            } else {
                $this->page = 1;
            }

            $this->countPages = ceil($this->count / $this->perPage);
            if ($this->page > $this->countPages) {
                $this->page = $this->countPages;
            }

            $this->offset = ($this->page - 1) * $this->perPage;

            // ссылка на предыдущую страницу
            if ($this->page > 1) {
                $get = $_GET;
                $get[$this->pageNumGetName] = $this->page - 1;
                $this->prevUrl = getCurrentUrl().'?'.http_build_query($get);
            }

            // ссылка на следующую страницу
            if ($this->page < $this->countPages) {
                $get = $_GET;
                $get[$this->pageNumGetName] = $this->page + 1;
                $this->nextUrl = getCurrentUrl().'?'.http_build_query($get);
            }
        }
    }

    public function getPageUrl($nPage) {
        $get = $_GET;
        $get[$this->pageNumGetName] = $nPage;
        return getCurrentUrl().'?'.http_build_query($get);
    }

    public function draw() {
        view_simply($this->view, ['paginationModel' => $this]);
    }
}