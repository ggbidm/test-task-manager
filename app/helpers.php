<?php

// выводит шаблон через лэйаут
function view($tpl, $params = [])
{
    extract($params);
    include('views/layout.tpl.php');
    exit();
}


// выводит шаблон минуя лэйаут
function view_simply($tpl, $params = [])
{
    extract($params);
    $tplFile = sprintf('views/%s.tpl.php', $tpl);
    include($tplFile);
}

function getOrderLink($fieldName, $fieldTitle) {
    $orderBy = isset($_GET['order_by'])?$_GET['order_by']:'id';
    $order = isset($_GET['order'])?$_GET['order']:'asc';
    $get = $_GET;
    $icon = '';

    if ($orderBy == $fieldName) {
        if ($order == 'asc') {
            $get['order'] = 'desc';
            $icon = ' ↓';
        } else {
            $get['order'] = 'asc';
            $icon = ' ↑';
        }
    } else {
        $get['order_by'] = $fieldName;
        $get['order'] = 'asc';
    }

    $url = getCurrentUrl().'?'.http_build_query($get);

    $link = sprintf('<a href="%s">%s</a>%s', $url, $fieldTitle, $icon);
    return $link;
}

function getCurrentUrl() {
    $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    $url = $url[0];

    return $url;
}

function csrf_input()
{
    return sprintf('<input name="csrf_token" type="hidden" value="%s" />', get_csrf());
}

function get_csrf()
{
    return $_SESSION['csrf_token'] = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'), 0, 30);
}

function check_csrf()
{
    return (isset($_SESSION['csrf_token']) && $_SESSION['csrf_token'] == @$_POST['csrf_token']);
}

function pre($a)
{
    echo '<pre>';
    print_r($a);
    die;
}

function vd($a)
{
    echo '<pre>';
    var_dump($a);
    die;
}

function redirect($url)
{
    header("Location: " . $url);
    exit();
}