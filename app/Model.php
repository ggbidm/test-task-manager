<?php

namespace App;

use DB;

class Model
{
    const TABLE = 'tasks';
    const PER_PAGE = 3;

    public $id;
    protected $saveFields = [];
    protected $fields = [];
    protected $errors = [];

    // Обязательные
    protected $requiredFields = [];

    // Email-поле для валидации
    protected $emailFields = [];

    protected $fieldNames = [];
    protected $tplRequiredErr = 'Поле <b>"%s"</b> обязательно.';
    protected $tplEmailErr = 'Поле <b>"%s"</b> должно быть email-адресом.';

    public static function getPagination($orderBy, $order) {
        $count = DB::getCount(self::TABLE);
        $pagination = new Pagination(self::PER_PAGE, $count);


        $pagination->list = DB::getAll(
            self::TABLE,
            $orderBy,
            $order,
            $pagination->offset,
            self::PER_PAGE
        );

        return $pagination;
    }

    public function load($fields)
    {
        foreach ($this->saveFields as $fieldName) {
            if (isset($fields[$fieldName]) && isset($this->{$fieldName})) {
                $this->{$fieldName} = $fields[$fieldName];
            }
        }

        return $this;
    }

    public function save() {
        $params = [];
        foreach ($this->fields as $field) {
            // сделал защиту от XSS тут ВСЕХ полей (для этого конкретного случая допустимо)
            if (isset($this->{$field})) $params[$field] = strip_tags($this->{$field});
        }

        if ($this->id) {
            DB::update(self::TABLE, $params, 'id = '.$this->id);
        } else {
            $this->id = DB::insert(self::TABLE, $params);
        }

        return $this;
    }

    public function validate() {
        foreach ($this->requiredFields as $reqField) {
            if (isset($this->$reqField)) {
                if (empty($this->$reqField)) {
                    $this->addError($reqField, sprintf($this->tplRequiredErr, $this->getFieldName($reqField)));
                }
            }
        }

        foreach ($this->emailFields as $emailField) {
            if (isset($this->$emailField)) {
                if (!filter_var($this->$emailField, FILTER_VALIDATE_EMAIL)) {
                    $this->addError($emailField, sprintf($this->tplEmailErr, $this->getFieldName($emailField)));
                }
            }
        }

        return $this;
    }

    public function getFieldName($field) {
        return isset($this->fieldNames[$field]) ? $this->fieldNames[$field] : $field;
    }

    public function addError($field, $text) {
        $this->errors[$field][] = $text;
    }

    public function isError($field=false) {
        if ($field) {
            return (isset($this->errors[$field]) && (count($this->errors[$field]) > 0));
        } else {
            return (count($this->errors) > 0);
        }
    }

    public function errors($field=false) {
        if ($field) {
            if (!isset($this->errors[$field])) return [];
            return $this->errors[$field];
        } else {
            return $this->errors;
        }
    }
}