<form method="post" class="form-signin">
    <?=csrf_input()?>
    <?php if ($user->isError('error')) { ?>
        <?php foreach ($user->errors('error') as $error) { ?>
            <div class="invalid-feedback" style="display: block;">
                <?=$error?>
            </div>
        <?php } ?>
    <?php } ?>
    <div class="form-label-group">
        <label for="login">Логин</label>
        <input name="login" type="text" id="inputEmail" class="form-control" placeholder="" autofocus>
        <?php if ($user->isError('login')) { ?>
            <?php foreach ($user->errors('login') as $error) { ?>
                <div class="invalid-feedback" style="display: block;">
                    <?=$error?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>

    <div class="form-label-group">
        <label for="password">Пароль</label>
        <input name="password" type="password" id="password" class="form-control" placeholder="">
        <?php if ($user->isError('password')) { ?>
            <?php foreach ($user->errors('password') as $error) { ?>
                <div class="invalid-feedback" style="display: block;">
                    <?=$error?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <button class="btn btn-lg btn-primary btn-block mt-4" type="submit">Войти</button>
</form>