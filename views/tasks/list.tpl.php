<h1>Список задач</h1>
<?php view_simply('messages'); ?>
<table class="table">
    <thead>
    <tr>
        <th scope="col">
            <?=getOrderLink('user_name', 'Имя пользователя')?>
        </th>
        <th scope="col">
            <?=getOrderLink('email', 'EMail')?>
        </th>
        <th scope="col">Текст задачи</th>
        <th scope="col">
            <?=getOrderLink('status', 'Статус')?>
        </th>

        <?php if (App\App::user()->isAuth) { ?>
            <th></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody>
        <?php if ($pagination->count > 0) { ?>
            <?php foreach ($pagination->list as $item) { ?>
                <tr>
                    <td><?=$item['user_name']?></td>
                    <td><?=$item['email']?></td>
                    <td><?=$item['description']?></td>
                    <td><?=($item['status']) ? 'Выполнено' : 'Не выполнено'?><?php if ($item['is_admin_updated']) { ?>, Отредактировано администратором<?php } ?></td>
                    <?php if (App\App::user()->isAuth) { ?>
                        <td>
                            <a class="btn btn-primary" href="/task/<?=$item['id']?>">Редактировать</a>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <th colspan="4">
                    Список пуст.
                </th>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div class="row">
    <div class="col-md-12 mb-3">
        <?=$pagination->draw()?>
    </div>
</div>

<div class="row jumbotron">
    <div class="col-md-12 order-md-1">
        <?php if ($message) { ?>
            <div class="alert alert-success fade show" role="alert">
                <?=$message?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>
        <form method="post" action="/" class="needs-validation" novalidate="">
            <?=csrf_input()?>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="user_name">Имя</label>
                    <input type="text" class="form-control" name="user_name" id="user_name" placeholder="" value="<?=$task->user_name?>" required="">
                    <?php if ($task->isError('user_name')) { ?>
                        <?php foreach ($task->errors('user_name') as $error) { ?>
                            <div class="invalid-feedback" style="display: block;">
                                <?=$error?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="email">EMail</label>
                    <input type="text" class="form-control" name="email" id="email" value="<?=$task->email?>">
                    <?php if ($task->isError('email')) { ?>
                        <?php foreach ($task->errors('email') as $error) { ?>
                            <div class="invalid-feedback" style="display: block;">
                                <?=$error?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="description">Текст задачи</label>
                    <textarea id="description" name="description" class="form-control"><?=$task->description?></textarea>
                    <?php if ($task->isError('description')) { ?>
                        <?php foreach ($task->errors('description') as $error) { ?>
                            <div class="invalid-feedback" style="display: block;">
                                <?=$error?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mb-3">
                    <input name="add_task" class="btn btn-primary btn-lg btn-block" type="submit" class="form-control" value="Добавить задачу"/>
                </div>
            </div>
        </form>
    </div>
</div>

