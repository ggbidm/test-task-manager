<h1>Редактирование задачи</h1>
<?php view_simply('messages'); ?>
<div class="row">
    <div class="col-md-12 order-md-1">
        <form method="post" class="needs-validation" novalidate="">
            <?=csrf_input()?>
            <div class="form-group">
                <label for="user_name">Статус</label>
                <div class="form-check">
                    <input type="hidden" name="status" value="0" />
                    <input value="1" <?=($task->status)?'checked':''?> name="status" type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Выполнено</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="user_name">Имя</label>
                    <input type="text" class="form-control" name="user_name" id="user_name" placeholder="" value="<?=$task->user_name?>" required="">
                    <?php if ($task->isError('user_name')) { ?>
                        <?php foreach ($task->errors('user_name') as $error) { ?>
                            <div class="invalid-feedback" style="display: block;">
                                <?=$error?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="email">EMail</label>
                    <input type="text" class="form-control" name="email" id="email" value="<?=$task->email?>">
                    <?php if ($task->isError('email')) { ?>
                        <?php foreach ($task->errors('email') as $error) { ?>
                            <div class="invalid-feedback" style="display: block;">
                                <?=$error?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="description">Текст задачи</label>
                    <textarea id="description" name="description" class="form-control"><?=$task->description?></textarea>
                    <?php if ($task->isError('description')) { ?>
                        <?php foreach ($task->errors('description') as $error) { ?>
                            <div class="invalid-feedback" style="display: block;">
                                <?=$error?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <input name="update_task" class="btn btn-primary btn-lg btn-block" type="submit" class="form-control" value="Сохранить"/>
                </div>
                <div class="col-md-6 mb-3">
                    <input name="update_task" class="btn btn-primary btn-lg btn-block" type="submit" class="form-control" value="Сохранить и перейти к списку"/>
                </div>
            </div>
        </form>
    </div>
</div>