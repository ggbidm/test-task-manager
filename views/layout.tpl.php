<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Задачи</title>

    <!-- Bootstrap core CSS -->
    <link href="/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/css.css" rel="stylesheet">
</head>

<body>
    <header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="/">Задачи</a>
            <?php if (\App\App::user()->isAuth) { ?>
                <a href="/logout" class="btn btn-warning" type="submit">Выйти</a>
            <?php } else { ?>
                <a href="/login" class="btn btn-success" type="submit">Авторизация</a>
            <?php } ?>
        </div>
    </nav>
    </header>

    <main role="main" class="container">
        <?php include(sprintf('views/%s.tpl.php', $tpl)); ?>
    </main>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="/public/js/bootstrap.min.js"></script>

</body>
</html>
