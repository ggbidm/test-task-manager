<?php if ($message = \App\App::getMessage()) { ?>
    <div class="alert alert-success fade show" role="alert">
        <?=$message?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>