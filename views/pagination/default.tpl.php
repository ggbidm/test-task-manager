<?php if ($paginationModel->countPages > 1) { ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item <?php if (!$paginationModel->prevUrl) { ?>disabled<?php } ?>">
                <a class="page-link" href="<?=$paginationModel->prevUrl?>" tabindex="-1">Предыдущая</a>
            </li>

            <?php for ($p = 1; $p <= $paginationModel->countPages; $p++) { ?>
                <li class="page-item <?php if($p == $paginationModel->page) { ?>disabled<?php } ?>">
                    <a class="page-link" href="<?=$paginationModel->getPageUrl($p)?>"><?=$p?></a>
                </li>
            <?php } ?>

            <li class="page-item <?php if (!$paginationModel->nextUrl) { ?>disabled<?php } ?>">
                <a class="page-link" href="<?=$paginationModel->nextUrl?>">Следующая</a>
            </li>
        </ul>
    </nav>
<?php } ?>