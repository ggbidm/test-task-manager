<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/app/DB.php';
require __DIR__ . '/app/helpers.php';

DB::connect();
session_start();

$router = new \Bramus\Router\Router();

// список задач
$router->get('/', function() {
    $controller = new \App\Controllers\TaskController();
    $controller->index();
});

// страница редактирования задачи
$router->get('/task/(\d+)', function($id) {
    $controller = new \App\Controllers\TaskController();
    $controller->edit($id);
});

// сохранение задачи
$router->post('/task/(\d+)', function($id) {
    $controller = new \App\Controllers\TaskController();
    $controller->edit($id);
});

// добавление задачи
$router->post('/', function() {
    $controller = new \App\Controllers\TaskController();
    $controller->index();
});

// страница авторизации
$router->get('/login', function() {
    $controller = new \App\Controllers\AuthController();
    $controller->login();
});

// авторизация
$router->post('/login', function() {
    $controller = new \App\Controllers\AuthController();
    $controller->login();
});

// выход
$router->get('/logout', function() {
    $controller = new \App\Controllers\AuthController();
    $controller->logout();
});

$router->run();